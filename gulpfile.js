'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cssnano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var sassGlob = require('gulp-sass-glob');
var rename = require("gulp-rename");
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var named = require('vinyl-named');
var uglify = require('gulp-uglify');
var babel = require('gulp-babel');
var gulpwebpack = require('webpack-stream');
var webpack = require('webpack');
var BowerWebpackPlugin = require("bower-webpack-plugin");

gulp.task('styles', function () {
	gulp.src(['./src/scss/config.scss'])
		.pipe(sassGlob())
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 5 versions'],
			cascade: false
		}))
		.pipe(cssnano())
		.pipe(rename('styles.css'))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./dist/css/'))
	.pipe(notify({ message: 'Styles task complete' }))
});

gulp.task('scripts', function(){
  return gulp.src('src/js/entry.js')
    .pipe(plumber({
      errorHandler: function (error) {
        notify({ message: 'Error!' });
        console.log(error.message);
        this.emit('end');
    }}))
    .pipe(named(function(file) {
      return 'bundle';
    }))
    .pipe(gulpwebpack({
        module: {
          loaders: [],
        },
        plugins: [
          new BowerWebpackPlugin()
          //, new webpack.optimize.UglifyJsPlugin({minimize: true})
        ]
      }))
    .pipe(babel())
    //.pipe(uglify())
    .pipe(gulp.dest('dist/js'))
    .pipe(notify({ message: 'Scripts task complete' }))
});

gulp.task('default', function () {
	gulp.watch('src/scss/**/*.scss', ['styles']);
	gulp.watch('src/js/*.js', ['scripts']);
});
