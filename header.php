<div class=" p:2  |  @m-h:6  @m-d:flex  @m-ai:center  @m-jc:center " style="background-color: #f8aa00">
	<div class="w:6col  @m-w:2col">
		<?php include 'header-logo.php'; ?>
	</div>
	<div class="@m-d:none  mt:6">
		<p class="f:$h1  @m-f:$body">
			House Café lorem ipsum dolor sit amet, consec tetur adipiscing elit. Sed faucibus dictum lacinia. Fusce semper felis odio.
		</p>
		<div class="mt:6">
			<input type="text" name="" value="" placeholder="EMAIL ADDRESS">
			<input type="submit" name="" value="SUBSCRIBE">
		</div>
		<div class="mt:6  d:flex  jc:space-between">
			<div  class="c:$white  mt:3">
				<?php include 'social.php'; ?>
			</div>
			<div class="w:3col  mt:2">
				<?php include 'villagelogo.php'; ?>
			</div>
		</div>

	</div>
</div>
