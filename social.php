<div class="d:flex  ai:center">


	

		

			<!-- Facebook -->
			<div class="pr:1 d:block">
				<a href="{{link.link}}" class="d:block  &hover:$opacity" target="_blank"><svg class="w:2 h:auto va:middle" style="vertical-align:middle" version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
					 x="0px" y="0px"  viewBox="0 0 12 12" style="enable-background:new 0 0 12 12;" xml:space="preserve">

				<defs>
				</defs>
				<path id="White_1_" fill="currentColor" d="M11.3,0L0.7,0C0.3,0,0,0.3,0,0.7l0,10.6c0,0.4,0.3,0.7,0.7,0.7l5.7,0l0-4.6l-1.6,0l0-1.8l1.6,0
					l0-1.3c0-1.5,0.9-2.4,2.3-2.4c0.7,0,1.2,0.1,1.4,0.1l0,1.6l-1,0c-0.7,0-0.9,0.4-0.9,0.9l0,1.1l1.8,0L9.8,7.3l-1.5,0l0,4.6l3,0
					c0.4,0,0.7-0.3,0.7-0.7l0-10.6C12,0.3,11.7,0,11.3,0z"/>
				</svg></a>
			</div>





		

		<!-- Instagram  -->
			<div class="pr:1 d:block">
				<a href="{{link.link}}" class="d:block  &hover:$opacity" target="_blank"><svg class="w:2 h:auto va:middle" style="vertical-align:middle" version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
					 x="0px" y="0px"  viewBox="0 0 13.8 13.8" style="enable-background:new 0 0 13.8 13.8;"
					 xml:space="preserve">

				<defs>
				</defs>
				<g>
					<path fill="currentColor" d="M6.9,1.2c1.8,0,2.1,0,2.8,0c0.7,0,1,0.1,1.3,0.2c0.3,0.1,0.6,0.3,0.8,0.5c0.2,0.2,0.4,0.5,0.5,0.8
						c0.1,0.2,0.2,0.6,0.2,1.3c0,0.7,0,0.9,0,2.8s0,2.1,0,2.8c0,0.7-0.1,1-0.2,1.3c-0.1,0.3-0.3,0.6-0.5,0.8c-0.2,0.2-0.5,0.4-0.8,0.5
						c-0.2,0.1-0.6,0.2-1.3,0.2c-0.7,0-0.9,0-2.8,0c-1.8,0-2.1,0-2.8,0c-0.7,0-1-0.1-1.3-0.2C2.5,12.1,2.3,12,2,11.7
						c-0.2-0.2-0.4-0.5-0.5-0.8c-0.1-0.2-0.2-0.6-0.2-1.3c0-0.7,0-0.9,0-2.8s0-2.1,0-2.8c0-0.7,0.1-1,0.2-1.3C1.7,2.5,1.8,2.3,2.1,2
						c0.2-0.2,0.5-0.4,0.8-0.5c0.2-0.1,0.6-0.2,1.3-0.2C4.9,1.2,5.1,1.2,6.9,1.2 M6.9,0C5,0,4.8,0,4.1,0c-0.7,0-1.2,0.1-1.7,0.3
						C1.9,0.5,1.6,0.8,1.2,1.1C0.8,1.5,0.6,1.9,0.4,2.4C0.2,2.8,0.1,3.3,0.1,4C0,4.8,0,5,0,6.9C0,8.7,0,9,0,9.7c0,0.7,0.1,1.2,0.3,1.7
						c0.2,0.5,0.4,0.8,0.8,1.2c0.4,0.4,0.8,0.6,1.2,0.8c0.4,0.2,0.9,0.3,1.7,0.3c0.7,0,1,0,2.8,0.1s2.1,0,2.8,0c0.7,0,1.2-0.1,1.7-0.3
						c0.5-0.2,0.8-0.4,1.2-0.8c0.4-0.4,0.6-0.8,0.8-1.2c0.2-0.4,0.3-0.9,0.3-1.7c0-0.7,0-1,0.1-2.8c0-1.9,0-2.1,0-2.8
						c0-0.7-0.1-1.2-0.3-1.7c-0.2-0.5-0.4-0.8-0.8-1.2c-0.4-0.4-0.8-0.6-1.2-0.8C11,0.2,10.5,0.1,9.8,0.1C9,0,8.8,0,6.9,0L6.9,0z"/>
					<path fill="currentColor" d="M6.9,3.4c-2,0-3.5,1.6-3.6,3.5c0,2,1.6,3.5,3.5,3.6s3.5-1.6,3.6-3.5S8.9,3.4,6.9,3.4z M6.9,9.2
						c-1.3,0-2.3-1-2.3-2.3s1-2.3,2.3-2.3s2.3,1,2.3,2.3S8.2,9.2,6.9,9.2z"/>
					<ellipse transform="matrix(3.218488e-03 -1 1 3.218488e-03 7.3232 13.7999)" fill="currentColor" cx="10.6" cy="3.2" rx="0.8" ry="0.8"/>
				</g>
				</svg></a>
		</div>
		 



		

			<!-- Twitter  -->
			<div class="pr:1 d:block">
				<a href="{{link.link}}" target="_blank" class=" &hover:$opacity">
				<svg class="w:2 h:auto va:middle" style="vertical-align:middle" version="1.1"
					 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
					 x="0px" y="0px" viewBox="0 0 14.9 12" style="enable-background:new 0 0 14.9 12;"
					 xml:space="preserve">

				<defs>
				</defs>
				<path fill="currentColor" d="M4.7,12c5.6,0,8.7-4.6,8.7-8.6c0-0.1,0-0.3,0-0.4c0.6-0.4,1.1-1,1.5-1.6c-0.5,0.2-1.1,0.4-1.7,0.5
					c0.6-0.4,1.1-1,1.3-1.7c-0.6,0.3-1.2,0.6-1.9,0.7c-0.6-0.6-1.3-1-2.2-1c-1.7,0-3,1.4-3.1,3c0,0.2,0,0.5,0.1,0.7
					C4.8,3.6,2.6,2.4,1.1,0.5C0.8,1,0.6,1.5,0.6,2.1c0,1.1,0.5,2,1.3,2.5c-0.5,0-1-0.2-1.4-0.4c0,0,0,0,0,0c0,1.5,1,2.7,2.4,3
					C2.8,7.3,2.5,7.3,2.2,7.3c-0.2,0-0.4,0-0.6-0.1c0.4,1.2,1.5,2.1,2.8,2.1c-1,0.8-2.4,1.3-3.8,1.3c-0.2,0-0.5,0-0.7,0
					C1.3,11.5,2.9,12,4.7,12"/>
				</svg></a>
			</div>




		

		<!-- Linkedin  -->
		<div class="pr:1 d:block">
			<a href="{{link.link}}"  class="d:block  &hover:$opacity"  target="_blank">
			<svg class="w:2 h:auto va:middle" style="vertical-align:middle" version="1.1"
				 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
				 x="0px" y="0px"  viewBox="0 0 13.2 13.2" style="enable-background:new 0 0 13.2 13.2;"
				 xml:space="preserve">

			<defs>
			</defs>
			<g>
				<g>
					<g>
						<path fill="currentColor" d="M0,12.4C0,8.5,0,4.7,0,0.8c0,0,0,0,0-0.1C0.1,0.3,0.3,0.1,0.7,0c0,0,0,0,0.1,0c3.9,0,7.8,0,11.7,0
							c0,0,0.1,0,0.1,0c0.4,0.1,0.6,0.3,0.7,0.7c0,0,0,0.1,0,0.1c0,3.9,0,7.8,0,11.7c0,0,0,0,0,0c-0.1,0.5-0.4,0.7-0.9,0.7
							c-3.2,0-6.4,0-9.6,0c-0.6,0-1.2,0-1.8,0C0.4,13.2,0.1,12.9,0,12.4C0,12.5,0,12.5,0,12.4z M10.3,10.3c0,0,0-0.1,0-0.1
							c0-1.1,0-2.2,0-3.4c0-0.4-0.1-0.7-0.2-1.1c-0.1-0.6-0.5-1-1.1-1.2c-0.3-0.1-0.6-0.1-1,0C7.6,4.6,7.2,4.7,7,5.2C7,5,7,4.8,7,4.5
							c-0.6,0-1.1,0-1.7,0c0,1.9,0,3.9,0,5.8c0.6,0,1.1,0,1.7,0c0-0.1,0-0.1,0-0.2c0-1,0-2,0-3c0-0.1,0-0.2,0-0.3
							c0-0.3,0.3-0.5,0.6-0.6c0.1,0,0.3,0,0.4,0c0.3,0,0.5,0.2,0.5,0.5c0,0.1,0,0.2,0,0.4c0,1,0,2.1,0,3.1c0,0.1,0,0.1,0,0.2
							C9.2,10.3,9.8,10.3,10.3,10.3z M4.5,10.3c0-1.9,0-3.9,0-5.8c-0.6,0-1.1,0-1.6,0c0,1.9,0,3.9,0,5.8C3.4,10.3,4,10.3,4.5,10.3z
							 M3.7,2.5c-0.5,0-0.8,0.4-0.8,0.8c0,0.5,0.4,0.8,0.8,0.8c0.5,0,0.8-0.4,0.8-0.8C4.6,2.8,4.2,2.5,3.7,2.5z"/>
					</g>
				</g>
			</g>
			</svg>
			</a>
		 </div>



	

 
</div>
