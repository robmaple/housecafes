<!doctype html>
<!--[if lt IE 7]><html class="no-js ie ie6 lt-ie9 lt-ie8 lt-ie7" lang="en-GB"> <![endif]-->
<!--[if IE 7]><html class="no-js ie ie7 lt-ie9 lt-ie8" lang="en-GB"> <![endif]-->
<!--[if IE 8]><html class="no-js ie ie8 lt-ie9" lang="en-GB"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en-GB"> <!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <title>
                    About
            </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="pingback" href="http://wordpress-4334-9752-204080.cloudwaysapps.comxmlrpc.php" />

    <meta name='robots' content='noindex,follow' />
		<link rel='dns-prefetch' href='//railhouse-template.dev' />
		<link rel='dns-prefetch' href='//s.w.org' />

		<script type='text/javascript' src='http://wordpress-4334-9752-204080.cloudwaysapps.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
		<script type='text/javascript' src='http://wordpress-4334-9752-204080.cloudwaysapps.com/wp-includes/js/jquery/jquery-migrate.js?ver=1.4.1'></script>
		<script type='text/javascript' src='http://wordpress-4334-9752-204080.cloudwaysapps.com/wp-content/themes/railhouse-template/dist/js/bundle.js?ver=4.7'></script>
		<link rel='https://api.w.org/' href='http://railhouse-template.dev/wp-json/' />
		<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://wordpress-4334-9752-204080.cloudwaysapps.comxmlrpc.php?rsd" />
		<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://wordpress-4334-9752-204080.cloudwaysapps.com/wp-includes/wlwmanifest.xml" />
		<meta name="generator" content="WordPress 4.7" />
		<link rel="canonical" href="http://railhouse-template.dev/about/" />
		<link rel='shortlink' href='http://railhouse-template.dev/?p=14' />
		<link rel="alternate" type="application/json+oembed" href="http://railhouse-template.dev/wp-json/oembed/1.0/embed?url=http%3A%2F%2Frailhouse-template.dev%2Fabout%2F" />
		<link rel="alternate" type="text/xml+oembed" href="http://railhouse-template.dev/wp-json/oembed/1.0/embed?url=http%3A%2F%2Frailhouse-template.dev%2Fabout%2F&#038;format=xml" />




    <link rel="stylesheet" href="/dist/css/styles.css">

		<body class="h:100%">

<style type="text/css">
		* {
			margin: 0;
			padding: 0;
		}

		#clock {
			position: relative;
			transform: translate(-2px, -100px);
			list-style: none;
			}
		#clock:after {
			content: '';
			width: 20px;
			height: 20px;
			border-radius: 1000px;
			background-color: white;
			position: absolute;
			top: 100px;
			left: 50%;
			margin-left: -10px;
			margin-top: -10px;
		}

		#sec, #min, #hour {
			position: absolute;
			width: 4px;
			height: 200px;
			}
		#sec:before, #min:before, #hour:before {
			position: relative;
			width: 4px;
			height: 100px;
			content: '';
			display: block;
			background-color: white;
			border-radius: 1000px;
		}

		#sec {

			z-index: 3;
				}

		#min:after {

			z-index: 2;
				}

		#hour:before {
			height: 50px;
			margin-top: 50px;
			z-index: 1;
				}
</style>

<script type="text/javascript">

		(($) => {

		$(document).ready(function() {

					setInterval( function() {
					var seconds = new Date().getSeconds();
					var sdegree = seconds * 6;
					var srotate = "rotate(" + sdegree + "deg)";

					$("#sec").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});

					}, 1000 );


					setInterval( function() {
					var hours = new Date().getHours();
					var mins = new Date().getMinutes();
					var hdegree = hours * 30 + (mins / 2);
					var hrotate = "rotate(" + hdegree + "deg)";

					$("#hour").css({"-moz-transform" : hrotate, "-webkit-transform" : hrotate});

					}, 1000 );


					setInterval( function() {
					var mins = new Date().getMinutes();
					var mdegree = mins * 6;
					var mrotate = "rotate(" + mdegree + "deg)";

					$("#min").css({"-moz-transform" : mrotate, "-webkit-transform" : mrotate});

					}, 1000 );

		});
		})(jQuery)

</script>

<div class="d:none  @m-d:flex  fxd:column  h:100vh  w:12col">

	<div class="w:3col  mx:auto pt:4  ta:center">
		<?php include 'header-logo2.php'; ?>
	</div>

</div>

<div class="d:flex  fxd:column  h:100vh  w:12col">

	<?php include 'header.php'; ?>



	<div class="pos:relative |  @m-fx:auto  @m-d:flex  @m-fxd:column  ">

		<?php include 'clock.php'; ?>

		<div class="w:12col    @m-d:flex   ai:stretch   fx:auto"  style="flex-basis:10px">

			<div class="w:12col  d:flex  jc:center  ai:center  |  @m-w:6col  " style="background-color:#0f98a9">
				<div class="p:4  w:9col">
          <a href="http://village.london"  class="&hover:$opacity">
					  <?php include 'railhouselogo.php'; ?>
          </a>
				</div>
			</div>

			<div class="w:12col  d:flex  jc:center  ai:center  |  @m-w:6col  " style="background-color:#ff5e17">
				<div class="p:4  w:9col">
          <a href="http://village.london"  class="&hover:$opacity">
					  <?php include 'ridinghouselogo.php'; ?>
          </a>
				</div>
			</div>

		</div>
		<div class="w:12col   @m-d:flex  fx:auto" style="flex-basis:10px">

			<div class="w:12col  d:flex  jc:center  ai:center  |  @m-w:6col  " style="background-color:#706f6f">
				<div class="p:4    w:9col">
          <a href="http://village.london"  class="&hover:$opacity">
					  <?php include 'villageeast.php'; ?>
          </a>
				</div>
			</div>

			<div class="d:none  w:12col  |  @m-d:block  @m-w:6col  " style="background-color:#f8aa00">
				<div class="p:4  w:12col">
          <input type="text" name="" value="" placeholder="EMAIL ADDRESS">
          <input type="submit" name="" value="SUBSCRIBE">
          <div  class="c:$white  mt:2">
            <?php include 'social.php'; ?>
          </div>
          <div class="mx:auto  w:4col  mt:2">
            <a href="http://village.london"  class="&hover:$opacity">
              <?php include 'villagelogo.php'; ?>
            </a>
          </div>
				</div>

			</div>
		</div>
	</div>

</div>
